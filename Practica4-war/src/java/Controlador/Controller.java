/*
 *  Realizar una aplicación web que permita que el alumno pueda rellenar la solicitud de matrícula en la
 *  academia de idiomas a través de una página Web, rellenando un formulario con sus datos y el idioma/nivel
 *  en que desea inscribirse. El sistema guardará los datos personales de modo que sucesivas inscripciones utilice
 *  el DNI para completar los datos y solo añada los cursos adicionales solicitados.
 */
package Controlador;

import Despliegue.GestorAlumnoGrupoRemote;
import Dominio.Alumno;
import Dominio.Idioma;
import Dominio.Solicitud;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;
import servicio.NewWebService_Service;

/**
 * @author Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
 * @author David Marciel <david.marciel at alumnos.uva.es>
 */
@WebServlet(name = "AcademyController", urlPatterns = {"/Academia"})
public class Controller extends HttpServlet {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/NewWebService/NewWebService.wsdl")
    private NewWebService_Service service;
    
    @EJB
    private GestorAlumnoGrupoRemote gestorAlumnoGrupo;
    
    private String url;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession sesion = request.getSession();
   
        this.url = "/index.jsp";
        try {
            
            if (sesion.getAttribute("dni") == null) { // comienzo sesion
                String dni = request.getParameter("dni");
                System.out.println("DNI buscado: " + dni);
//                LoginFacade loginFachada = new LoginFacade();
//                Facade fachada = new Facade();
                Alumno alumno = gestorAlumnoGrupo.buscarAlumno(dni.toUpperCase());
                // Comprueba que sea un usuario registrado
//                if (loginFachada.isUser(dni)) {
                if ( alumno != null) {
                    System.out.println(alumno);
                    // si está registrado cargamos sus datos y le mandamos a la página de solicitudes.
//                    alumno = loginFachada.getAlumno(dni.toUpperCase());
//                    alumno = gestorAlumnoGrupo.buscarAlumno(dni);
                    sesion.setAttribute("dni", dni.toUpperCase());
                    sesion.setAttribute("alumno_registrado", true);
                    sesion.setAttribute("alumno", alumno);
                    
                    System.out.println("infoUsuario "+alumno.getApellidos()+" "+alumno.getDni());
                    
                    
                    
                    //ArrayList<Idioma> idiomas = fachada.getIdiomas();
                    List<Idioma> idiomas = gestorAlumnoGrupo.obtenerIdiomas();
                    //ArrayList<Solicitud> solicitudes = fachada.getSolicitudesAlumno(dni.toUpperCase());
                    List<Solicitud> solicitudes = gestorAlumnoGrupo.buscarSolicitudes(alumno);
                       
                    System.out.println("idiomas "+ idiomas.get(0) + " "+ idiomas.get(0).getIdioma() + idiomas.size());
                    System.out.println("idiomas " + idiomas);
                    sesion.setAttribute("idiomas",idiomas);
                    sesion.setAttribute("solicitudes",solicitudes);
                    this.url = "/solicitud.jsp";
                } else {                    
                    // si no está registrado le mandamos a la página de registro.
                    sesion.setAttribute("dni", dni.toUpperCase());
                    sesion.setAttribute("alumno_registrado", false);
                    this.url = "/registro.jsp";
                }
                sesion.setAttribute("siguienteURL", this.url);
            } else {  //sesion ya abierta
                String accion = request.getParameter("accion");
                if (accion == null) { // No se viene de formulario
                    this.url = (String) sesion.getAttribute("siguienteURL");
                } else { // Caso navegacion normal
//                    Facade fachada = new Facade();
//                    LoginFacade loginFachada = new LoginFacade();
                    switch (accion) {
                        // Registrar un alumno nuevo
                        case "registrar":
                            String dni = (String) sesion.getAttribute("dni");
                            dni = dni.toUpperCase();
                            String nombre = (String) request.getParameter("nombre");
                            String apellidos = (String) request.getParameter("apellidos");
                            String telefono = (String) request.getParameter("telefono");
                            String direccion = (String) request.getParameter("direccion");
                           
                            if((Boolean) sesion.getAttribute("alumno_registrado")){
                                // Si el alumno ya está registrado, se actualizan sus datos.
//                                fachada.actualizarAlumno(dni, nombre, apellidos, telefono, direccion);
//                                sesion.setAttribute("alumno",loginFachada.getAlumno(dni));
                                sesion.setAttribute("alumno",gestorAlumnoGrupo.buscarAlumno(dni));
                            } else {
                                // Si el alumno no está registrado, lo guardamos como nuevo alumno.
                                //fachada.registrarAlumno(dni, nombre, apellidos, telefono, direccion);
                                Alumno al = new Alumno();
                                al.setDni(dni);
                                al.setNombre(nombre);
                                al.setApellidos(apellidos);
                                al.setTelefono(telefono);
                                al.setDireccion(direccion);                                
                                gestorAlumnoGrupo.registrarAlumno(al);
                                
                                sesion.setAttribute("alumno_registrado",true);
//                                sesion.setAttribute("alumno",loginFachada.getAlumno(dni));
                                sesion.setAttribute("alumno",gestorAlumnoGrupo.buscarAlumno(dni));
                            }
                            //obtenemos las solicitudes del alumno y los idiomas posibles
//                            ArrayList<IdiomaDTO> idiomas = fachada.getIdiomas();
                            List<Idioma> idiomas = gestorAlumnoGrupo.obtenerIdiomas();
//                            ArrayList<SolicitudDTO> solicitudes = fachada.getSolicitudesAlumno(dni);
                            List<Solicitud> solicitudes = gestorAlumnoGrupo.buscarSolicitudes(gestorAlumnoGrupo.buscarAlumno(dni));
                            sesion.setAttribute("idiomas",idiomas);
                            sesion.setAttribute("solicitudes",solicitudes);
                            this.url="/solicitud.jsp";
                            sesion.setAttribute("siguienteURL", this.url);
                            break;
                        // Guardar solicitudes de un alumno
                        case "guardarSolicitudes":
//                            idiomas = fachada.getIdiomas();
                            idiomas = gestorAlumnoGrupo.obtenerIdiomas();
                            dni = (String) sesion.getAttribute("dni");
//                          for( IdiomaDTO idioma : idiomas) {
                            for( Idioma idioma : idiomas) {
                                List<Solicitud> solicitudesAlumno = gestorAlumnoGrupo.buscarSolicitudes(gestorAlumnoGrupo.buscarAlumno(dni));
                                if(request.getParameter("cb_"+idioma.getIdioma())!=null){ //Comprobamos los checkbox marcados.
                                    int nivel = new Integer(request.getParameter("sel_"+idioma.getIdioma()));
                                    for(Solicitud s: solicitudesAlumno) {
                                        if(s.getIdioma1().equals(idioma.getIdioma())) {
                                            // si la solicitud existe se actualiza
                                            // TODO No implementado en EJB
                                        } else {
                                            // si la solicitud no existe se crea
                                            // TODO No implementado en EJB
                                        }
                                    }
                                    
                                } else {
                                    for(Solicitud s: solicitudesAlumno) {
                                        if(s.getIdioma1().equals(idioma.getIdioma())) {
                                            // Si los checkbox no marcados eran solicitudes se borran del sistema.
                                            // TODO No implementado en EJB
                                        }
                                    }
                                }
                            }
//                            sesion.setAttribute("alumno_solicitudes", fachada.getSolicitudesAlumno(dni));
                            sesion.setAttribute("alumno_solicitudes", gestorAlumnoGrupo.buscarSolicitudes(gestorAlumnoGrupo.buscarAlumno(dni)));
                            
                            
                            this.url="/confirmacion.jsp";
                            sesion.setAttribute("siguienteURL", this.url);
                            break;
                        // Editar los datos de un alumno que ya está en el sistema.
                        case "editarDatos":
                            this.url = "/registro.jsp";
                            sesion.setAttribute("siguienteURL", this.url);
                            break;
                        // Finalizar sesion.
                        case "irInicio":
                            this.url="/index.jsp";
                            sesion.invalidate();
                            break;
                        case "cerrarSesion":
                            this.url = "/index.jsp";
                            sesion.invalidate();
                            break;
                        default:
                            this.url = "/index.jsp";
                            sesion.invalidate();
                            break;
                    }
                }
            }
        } catch (Exception e) {
            request.setAttribute("tipo", e);
            this.url = "/error.jsp?error=StdException";
            e.printStackTrace();
        } finally {
            
            RequestDispatcher respuesta = getServletContext().getRequestDispatcher(this.url);
            respuesta.forward(request, response);
        }
        }
    
        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    private String tipoEmpleado(java.lang.String dni) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        servicio.NewWebService port = service.getNewWebServicePort();
        return port.tipoEmpleado(dni);
    }

    private boolean identificar(java.lang.String dni, java.lang.String password) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        servicio.NewWebService port = service.getNewWebServicePort();
        return port.identificar(dni, password);
    }

    }
