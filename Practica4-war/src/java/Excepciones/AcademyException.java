/*
 *  Realizar una aplicación web que permita que el alumno pueda rellenar la solicitud de matrícula en la
 *  academia de idiomas a través de una página Web, rellenando un formulario con sus datos y el idioma/nivel
 *  en que desea inscribirse. El sistema guardará los datos personales de modo que sucesivas inscripciones utilice
 *  el DNI para completar los datos y solo añada los cursos adicionales solicitados.
 */
package Excepciones;

/**
 * @author Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
 * @author David Marciel <david.marciel at alumnos.uva.es>
 */
public class AcademyException extends Exception {
    
    private String mensaje;

    /**
     * Creates a new instance of
     * <code>MiTiendaException</code> without detail message.
     */
    public AcademyException() {
    }

    /**
     * Constructs an instance of
     * <code>MiTiendaException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public AcademyException(String msg) {
        this.mensaje = msg;
    }

    @Override
    public String toString() {
        return this.mensaje;
    }
    
    
}
