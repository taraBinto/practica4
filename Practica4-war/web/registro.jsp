<%-- 
    Document   : registro
    Created on : 03-nov-2014, 20:23:18
    Author     : Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
--%>

<%@page import="Dominio.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Academia Maroya. Ven a aprender idiomas.</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script language="Javascript" type="text/JavaScript">
            function validar() {
                var nombre = document.register.nombre.value;
                var apellidos = document.register.apellidos.value;
                var telefono = document.register.telefono.value;
                var direccion = document.register.direccion.value;
                if (!(nombre.length>0 && apellidos.length>0 && telefono.length>0 && direccion.length>0)) {
                    alert ("Rellene todos los datos.");
                    document.register.nombre.focus();
                    return false;
                }
                document.register.submit(); 
            }
        </script>
    </head>
    <body>
        <% 
            boolean registrado = (Boolean) session.getAttribute("alumno_registrado"); 
            Alumno alumno=null;
            if(registrado) {
                alumno = (Alumno) session.getAttribute("alumno"); 
            }
        %>
        <div class="col-md-1"></div>
        <div class="col-md-10">
            
            <!-- Barra de navegación-->
            <jsp:include page="navigation.jsp" />            
            <p>Introduzca sus datos.</p>
            <form class="form-horizontal" action="Academia" name="register" method="post">
                
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Nombre</label><br/>
                        <input class="form-control" type="text" name="nombre" placeholder="ej: Juan" value="<% if(registrado) out.print(alumno.getNombre());%>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Apellidos</label><br/>
                        <input class="form-control" type="text" name="apellidos" placeholder="ej: Pérez Martínez" value="<% if(registrado) out.print(alumno.getApellidos());%>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Teléfono</label><br/>
                        <input class="form-control" type="text" name="telefono" placeholder="ej: 983555555" value="<% if(registrado) out.print(alumno.getTelefono());%>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Dirección</label><br/>
                        <input class="form-control" type="text" name="direccion" placeholder="ej: C/ Me falta un tornillo 2" value="<% if(registrado) out.print(alumno.getDireccion());%>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <button class="btn btn-default" type="submit" value="Guardar" onclick="validar(); return false;">Guardar Datos</button>
                        <button class="btn btn-default" type="reset" value="Borrar">Borrar</button>
                        <input type="hidden" name="accion" value="registrar">
                    </div>
                </div>
            </form>
            
        </div>
        <div class="col-md-1"></div>
    </body>
</html>
