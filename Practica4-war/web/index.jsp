
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Academia Maroya. Ven a aprender idiomas.</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script language="Javascript" type="text/JavaScript">
            function validar() {
                var dni = document.dni_login.dni.value;
                if (alfanumerico(dni)==0 || !dni.length==10 ||dni.charAt(dni.length-2)!="-") {
                    alert ("Introduzca un DNI con el formato 00000000-X.");
                    document.dni_login.dni.focus();
                    return false;
                }
                /*if(!isDNI(dni.substr(0,dni.length-2).concat(dni.charAt(dni.length-1)))){
                    alert ("No es un dni valido.");
                    document.dni_login.dni.focus();
                    return false;
                }*/
                
                document.dni_login.submit();
            }

            function alfanumerico(txt) {
                // Devuelve 0 si la cadena esta vacia, 1 si es numerica 
                //o 2 si es alfanumerica
                var i;
                if (txt.length!=0) {
                    for (i=0;i<txt.length;i++){
                        if (txt.substr(i,1)<"0" || txt.substr(i,1)>"9") {
                            return 2;
                        }
                    }
                    return 1;
                }
                else
                    return 0;
            }
            // Comprueba si es un DNI correcto (entre 5 y 8 letras seguidas de la letra que corresponda).
            // Acepta NIEs (Extranjeros con X, Y o Z al principio)
            function isDNI(dni) {
                    var numero, let, letra;
                    var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

                    dni = dni.toUpperCase();

                    if(expresion_regular_dni.test(dni) === true){
                            numero = dni.substr(0,dni.length-1);
                            numero = numero.replace('X', 0);
                            numero = numero.replace('Y', 1);
                            numero = numero.replace('Z', 2);
                            let = dni.substr(dni.length-1, 1);
                            numero = numero % 23;
                            letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
                            letra = letra.substring(numero, numero+1);
                            if (letra != let) {
                                    //alert('Dni erroneo, la letra del NIF no se corresponde');
                                    return false;
                            }else{
                                    //alert('Dni correcto');
                                    return true;
                            }
                    }else{
                            //alert('Dni erroneo, formato no válido');
                            return false;
                    }
            }
        </script>

    </head>
    <body>
        <% if (!session.isNew()) {
            response.sendRedirect("Academia");
        }
        %>
        
        <div class="col-md-1"></div>
        <div class="col-md-10">
            
            <!-- Barra de navegación-->
            <jsp:include page="navigation.jsp" />
            <p>Introduzca su DNI para entrar en la academia.</p>
            <form class="form-horizontal" action="Academia" name="dni_login" method="post">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>DNI</label><br/>
                        <input class="form-control" type="text" name="dni" placeholder="00000000-X">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <button class="btn btn-default" type="submit" value="Entrar" onclick="validar(); return false;">Entrar</button>
                        <button class="btn btn-default" type="reset" value="Borrar">Borrar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-1"></div>
    </body>
</html>
