<%-- 
    Document   : navigation
    Created on : 03-nov-2014, 19:51:04
    Author     : Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
--%>

<%@page import="Dominio.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="page-header">
    <h1>Academia Maroya. <small>Ven a aprender idiomas.</small></h1>
</div>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" onclick="document.inicio.submit()"><span class="glyphicon glyphicon-home"></span> Inicio</a> 
            <form name="inicio" class="navbar-form navbar-left" action="Academia" role="form" method="post">
                <input type="hidden" name="accion" value="irInicio">
            </form>
        </div>
    </div>
</nav>
