<%-- 
    Document   : solicitud
    Created on : 03-nov-2014, 20:17:48
    Author     : Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
--%>

<%@page import="Dominio.*"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Academia Maroya. Ven a aprender idiomas.</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        
        <div class="col-md-1"></div>
        <div class="col-md-10">
            
            <!-- Barra de navegación-->
            <jsp:include page="navigation.jsp" />
            <%
                Alumno alumno = (Alumno) session.getAttribute("alumno");
            %>
            
            <div class="col-md-4">
            <!-- ficha alumno -->
                <h2>Datos personales.</h2>
                <dl>
                    <dt>DNI</dt>
                    <dd><%=alumno.getDni()%></dd>
                    <dt>Nombre</dt>
                    <dd><%=alumno.getNombre()%></dd>
                    <dt>Apellidos</dt>
                    <dd><%=alumno.getApellidos()%></dd>
                    <dt>Teléfono</dt>
                    <dd><%=alumno.getTelefono()%></dd>
                    <dt>Dirección</dt>
                    <dd><%=alumno.getDireccion()%></dd>
                </dl>
                <!--form class="form-horizontal" action="Academia" name="editar_datos" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <button class="btn btn-default" type="submit" value="Entrar">Editar Datos</button>
                            <input type="hidden" name="accion" value="editarDatos">
                        </div>
                    </div>
                </form-->
            </div>
            <div class="col-md-8">
                <!-- solicitudes -->
                <h2>Solicitudes.</h2>
                <form class="form-horizontal" action="Academia" name="solicitar_idiomas" method="post">
                    <table class="table table-condensed">
                        <thead>
                            <th></th>
                            <th>Idioma</th>
                            <th>Nivel</th>
                        </thead>
                        <%
                            List<Idioma> idiomas = (List<Idioma>) session.getAttribute("idiomas");
                            List<Solicitud> solicitudes = (List<Solicitud>) session.getAttribute("solicitudes");                            
                            
                            int numeroSolicitudes = solicitudes.size();
                            int indiceSolicitudes = 0,colorInt=0;
                            String colorStr;
                            Solicitud solicitud = null;
                            for(Idioma idioma : idiomas){
                                boolean inscrito = false;
                                int nivel=0;
                                
                                if(indiceSolicitudes<numeroSolicitudes){
                                    solicitud = solicitudes.get(indiceSolicitudes);
                                }
                                if(solicitud!=null) 
                                    if(solicitud.getIdioma1().getIdioma().equals(idioma.getIdioma())) {
                                        inscrito=true;
                                        nivel=solicitud.getNivel();
                                        indiceSolicitudes++;
                                    }
                                
                                colorInt=(colorInt+1)%2;
                                if(colorInt%2==0){
                                    colorStr="";
                                } else {
                                    colorStr="active";
                                }
                                 %>
                                 <tr class="<%=colorStr%>">
                                     <td>
                                         <input type="checkbox" name="cb_<%=idioma.getIdioma()%>" <% if(inscrito) { %>checked="checked"<% } %>>
                                     </td>
                                     <td><%=idioma.getIdioma()%></td>
                                     <td>
                                         <select name="sel_<%=idioma.getIdioma()%>">
                                             <% for(int i=1;i<=6;i++) { %>
                                             <option value="<%=i%>" <% if(inscrito && nivel==i) { %>selected<% } %>><%=i%></option>
                                             <% } %>
                                         </select>
                                     </td>
                                 </tr>
                                 <%
                            }
                        %>
                    </table>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button class="btn btn-default" type="submit" value="Entrar">Solicitar Cursos</button>
                            <button class="btn btn-default" type="reset" value="Borrar">Borrar</button>
                            <input type="hidden" name="accion" value="guardarSolicitudes">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-1"></div>
    </body>
</html>
