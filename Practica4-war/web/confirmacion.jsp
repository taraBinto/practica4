<%-- 
    Document   : confirmacion
    Created on : 04-nov-2014, 12:38:35
    Author     : Guillermo Goyanes <guillermo.goyanes at alumnos.uva.es>
--%>

<%@page import="Dominio.*"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Academia Maroya. Ven a aprender idiomas.</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        
        <div class="col-md-1"></div>
        <div class="col-md-10">
            
            <!-- Barra de navegación-->
            <jsp:include page="navigation.jsp" />
            <% Alumno alumno = (Alumno) session.getAttribute("alumno"); %>
            
            <div class="col-md-4">
            <!-- ficha alumno -->
                <h2>Datos personales.</h2>
                <dl>
                    <dt>DNI</dt>
                    <dd><%=alumno.getDni()%></dd>
                    <dt>Nombre</dt>
                    <dd><%=alumno.getNombre()%></dd>
                    <dt>Apellidos</dt>
                    <dd><%=alumno.getApellidos()%></dd>
                    <dt>Teléfono</dt>
                    <dd><%=alumno.getTelefono()%></dd>
                    <dt>Dirección</dt>
                    <dd><%=alumno.getDireccion()%></dd>
                </dl>
            </div>
            <div class="col-md-8">
                <h2>Solicitudes enviadas.</h2>
                <%
                    List<Solicitud> solicitudes = (List<Solicitud>) session.getAttribute("alumno_solicitudes");
                    if(solicitudes.size()>0) {
                %>
                <table class="table table-condensed">
                    <thead>
                        <th></th>
                        <th>Idioma</th>
                        <th>Nivel</th>
                    </thead>
                    <%
                        int colorInt=0;
                        String colorStr;
                        for(Solicitud solicitud : solicitudes){                            
                            colorInt=(colorInt+1)%2;
                            if(colorInt%2==0){
                                colorStr="";
                            } else {
                                colorStr="active";
                            }
                             %>
                             <tr class="<%=colorStr%>">
                                 <td>&nbsp;</td>
                                 <td><%=solicitud.getIdioma1().getIdioma()%></td>
                                 <td><%=solicitud.getNivel()%></td>
                             </tr>
                             <%
                        }
                    %>
                </table>
                
                <p>¡Gracias por confiar en nosotros!</p>
                <% } else { //Cuando no hay solicitudes %>
                <p>
                    No tenemos ninguna solitud suya para cursar idiomas. 
                    Por favor, seleccione alguna idioma y el nivel al que desea apuntarse
                </p>
                <% } %>
            </div>
        </div>
        <div class="col-md-1"></div>
    </body>
</html>
